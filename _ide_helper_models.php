<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Dungeon
 *
 * @property int                                                          $id
 * @property string                                                       $name
 * @property int                                                          $region_id
 * @property int                                                          $subregion_id
 * @property string                                                       $type
 * @property mixed                                                        $position
 * @property string                                                       $icon_id
 * @property \Carbon\Carbon|null                                          $created_at
 * @property \Carbon\Carbon|null                                          $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereIconId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereSubregionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Mission[] $missions
 * @property-read \App\Subregion                                          $subregion
 */
	class Dungeon extends \Eloquent {}
}

namespace App{
/**
 * App\Enhancement
 *
 * @property int                 $id
 * @property string              $name
 * @property int                 $skill_id_old
 * @property int                 $skill_id_new
 * @property mixed               $cost
 * @property mixed               $units
 * @property string              $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereSkillIdNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereSkillIdOld($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereUnits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Enhancement extends \Eloquent {}
}

namespace App{
/**
 * App\Equipment
 *
 * @property int                     $id
 * @property string                  $name
 * @property int                     $compendium_id
 * @property int                     $compendium_shown
 * @property int                     $rarity
 * @property int                     $type_id
 * @property int                     $slot_id
 * @property int                     $is_twohanded
 * @property mixed                   $dmg_variance
 * @property int                     $accuracy
 * @property string                  $requirement_type
 * @property int                     $requirement_id
 * @property mixed                   $skills
 * @property mixed                   $effects
 * @property mixed                   $stats
 * @property int                     $price_buy
 * @property int                     $price_sell
 * @property string                  $icon
 * @property mixed                   $strings
 * @property \Carbon\Carbon|null     $created_at
 * @property \Carbon\Carbon|null     $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereAccuracy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereCompendiumShown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereDmgVariance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereEffects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereIsTwohanded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment wherePriceBuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment wherePriceSell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereRarity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereRequirementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereRequirementType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereSkills($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereSlotId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereStats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\EquipmentSlot $slot
 * @property-read \App\EquipmentType $type
 */
	class Equipment extends \Eloquent {}
}

namespace App{
/**
 * App\EquipmentSlot
 *
 * @property int                 $id
 * @property string              $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentSlot whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentSlot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentSlot whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentSlot whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Equipment $equipment
 */
	class EquipmentSlot extends \Eloquent {}
}

namespace App{
/**
 * App\EquipmentType
 *
 * @property int                 $id
 * @property string              $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentType whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Equipment $equipment
 */
	class EquipmentType extends \Eloquent {}
}

namespace App{
/**
 * App\Expedition
 *
 * @property int                 $id
 * @property string              $name
 * @property int                 $type
 * @property int                 $cost
 * @property string              $rank
 * @property int                 $difficulty
 * @property int                 $duration
 * @property int                 $units
 * @property mixed               $required
 * @property int                 $next_id
 * @property mixed               $reward
 * @property int                 $relics
 * @property mixed               $exp_levels
 * @property mixed               $unit_bonus
 * @property int                 $unit_bonus_max
 * @property mixed               $item_bonus
 * @property mixed               $stat_bonus
 * @property mixed               $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereDifficulty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereExpLevels($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereItemBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereNextId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereRelics($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereStatBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereUnitBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereUnitBonusMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereUnits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Expedition extends \Eloquent {}
}

namespace App{
/**
 * Class Game
 *
 * @package App
 * @property int                                                       $id
 * @property string                                                    $name
 * @property \Carbon\Carbon|null                                       $created_at
 * @property \Carbon\Carbon|null                                       $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Unit[] $units
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Game extends \Eloquent {}
}

namespace App{
/**
 * App\Item
 *
 * @property int                 $id
 * @property string              $name
 * @property string              $type
 * @property int                 $compendium_id
 * @property int                 $compendium_shown
 * @property int                 $usable_in_combat
 * @property int                 $usable_in_exploration
 * @property mixed               $flags
 * @property int                 $carry_limit
 * @property int                 $stack_size
 * @property int                 $price_buy
 * @property int                 $price_sell
 * @property mixed               $effects
 * @property mixed               $effects_raw
 * @property string              $icon
 * @property mixed               $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCarryLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCompendiumShown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereEffects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereEffectsRaw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereFlags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item wherePriceBuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item wherePriceSell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereStackSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUsableInCombat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUsableInExploration($value)
 * @mixin \Eloquent
 */
	class Item extends \Eloquent {}
}

namespace App{
/**
 * App\Job
 *
 * @property int                                                       $id
 * @property string                                                    $name
 * @property \Carbon\Carbon|null                                       $created_at
 * @property \Carbon\Carbon|null                                       $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Unit[] $units
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Job extends \Eloquent {}
}

namespace App{
/**
 * App\Limitburst
 *
 * @property int                 $id
 * @property string              $name
 * @property int                 $cost
 * @property mixed               $attack_count
 * @property mixed               $attack_frames
 * @property mixed               $effect_frames
 * @property int                 $move_type
 * @property string              $damage_type
 * @property mixed               $element_inflict
 * @property int                 $levels
 * @property mixed               $min_level
 * @property mixed               $max_level
 * @property mixed               $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereAttackCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereAttackFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereDamageType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereEffectFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereElementInflict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereLevels($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereMaxLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereMinLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereMoveType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Limitburst extends \Eloquent {}
}

namespace App{
/**
 * App\Materia
 *
 * @property int                 $id
 * @property string              $name
 * @property int                 $compendium_id
 * @property mixed               $skills
 * @property mixed               $effects
 * @property int                 $unique
 * @property int                 $price_buy
 * @property int                 $price_sell
 * @property string              $icon
 * @property mixed               $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereEffects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia wherePriceBuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia wherePriceSell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereSkills($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereUnique($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Materia extends \Eloquent {}
}

namespace App{
/**
 * App\Mission
 *
 * @property int                 $id
 * @property int                 $dungeon_id
 * @property string              $name
 * @property string              $type
 * @property int                 $wave_count
 * @property string              $cost_type
 * @property int                 $cost
 * @property mixed               $flags
 * @property mixed               $reward
 * @property int                 $gil
 * @property int                 $exp
 * @property mixed               $challenges
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereChallenges($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereCostType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereDungeonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereFlags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereGil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereWaveCount($value)
 * @mixin \Eloquent
 * @property-read \App\Dungeon   $dungeon
 */
	class Mission extends \Eloquent {}
}

namespace App{
/**
 * App\Mogking
 *
 * @mixin \Eloquent
 */
	class Mogking extends \Eloquent {}
}

namespace App{
/**
 * App\Recipe
 *
 * @property int                 $id
 * @property string              $name
 * @property int                 $compendium_id
 * @property int                 $compendium_shown
 * @property string              $item
 * @property int                 $time
 * @property mixed               $mats
 * @property int                 $count
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereCompendiumShown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereMats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Recipe extends \Eloquent {}
}

namespace App{
/**
 * App\Region
 *
 * @property int                                                            $id
 * @property string                                                         $name
 * @property \Carbon\Carbon|null                                            $created_at
 * @property \Carbon\Carbon|null                                            $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Region whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Region whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Region whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subregion[] $subregions
 */
	class Region extends \Eloquent {}
}

namespace App{
/**
 * App\Sex
 *
 * @property int                                                       $id
 * @property string                                                    $name
 * @property \Carbon\Carbon|null                                       $created_at
 * @property \Carbon\Carbon|null                                       $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Unit[] $units
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Sex extends \Eloquent {}
}

namespace App{
/**
 * App\Skill
 *
 * @property int                 $id
 * @property string              $type
 * @property int                 $usable_in_exploration
 * @property string              $name
 * @property int                 $compendium_id
 * @property int                 $rarity
 * @property string              $magic_type
 * @property int                 $mp_cost
 * @property int                 $is_sealable
 * @property int                 $is_reflectable
 * @property mixed               $attack_count
 * @property mixed               $attack_frames
 * @property mixed               $effect_frames
 * @property int                 $move_type
 * @property mixed               $effect_type
 * @property string              $attack_type
 * @property mixed               $element_inflict
 * @property mixed               $effects
 * @property mixed               $effects_raw
 * @property mixed               $unit_restriction
 * @property string              $icon
 * @property mixed               $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereAttackCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereAttackFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereAttackType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereEffectFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereEffectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereEffects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereEffectsRaw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereElementInflict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereIsReflectable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereIsSealable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereMagicType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereMoveType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereMpCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereRarity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereUnitRestriction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereUsableInExploration($value)
 * @mixin \Eloquent
 */
	class Skill extends \Eloquent {}
}

namespace App{
/**
 * App\Subregion
 *
 * @property int                                                          $id
 * @property string                                                       $name
 * @property \Carbon\Carbon|null                                          $created_at
 * @property \Carbon\Carbon|null                                          $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subregion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subregion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subregion whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subregion whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Dungeon[] $dungeons
 * @property-read \App\Region                                             $region
 */
	class Subregion extends \Eloquent {}
}

namespace App{
/**
 * App\Summon
 *
 * @property int                                                              $id
 * @property mixed                                                            $names
 * @property string                                                           $image
 * @property string                                                           $icon
 * @property mixed                                                            $skill
 * @property mixed                                                            $color
 * @property mixed                                                            $entries
 * @property \Carbon\Carbon|null                                              $created_at
 * @property \Carbon\Carbon|null                                              $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereEntries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereSkill($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SummonBoard[] $summonBoard
 */
	class Summon extends \Eloquent {}
}

namespace App{
/**
 * App\SummonBoard
 *
 * @property int                 $id
 * @property int                 $summon_id
 * @property mixed               $node
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SummonBoard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SummonBoard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SummonBoard whereNode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SummonBoard whereSummonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SummonBoard whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Summon    $summon
 */
	class SummonBoard extends \Eloquent {}
}

namespace App{
/**
 * App\Unit
 *
 * @property int                                                            $id
 * @property int                                                            $rarity_min
 * @property int                                                            $rarity_max
 * @property string                                                         $name
 * @property mixed                                                          $names
 * @property int                                                            $game_id
 * @property int                                                            $job_id
 * @property int                                                            $sex_id
 * @property int                                                            $tribe_id
 * @property int                                                            $is_summonable
 * @property string                                                         $TMR_type
 * @property int                                                            $TMR_id
 * @property mixed                                                          $equip
 * @property mixed                                                          $skills
 * @property \Carbon\Carbon|null                                            $created_at
 * @property \Carbon\Carbon|null                                            $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereEquip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereGame($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereIsSummonable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereJobId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereRarityMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereRarityMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereSexId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereSkills($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereTMRId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereTMRType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereTribeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UnitEntry[] $entries
 * @property-read \App\Game                                                 $game
 * @property-read \App\Job                                                  $job
 * @property-read \App\Sex                                                  $sex
 */
	class Unit extends \Eloquent {}
}

namespace App{
/**
 * App\UnitEntry
 *
 * @property int                 $id
 * @property int                 $unit_id
 * @property int                 $compendium_id
 * @property int                 $rarity
 * @property int                 $growth_pattern
 * @property mixed               $stats
 * @property int                 $limitburst_id
 * @property int                 $attack_count
 * @property mixed               $attack_frames
 * @property mixed               $effect_frames
 * @property int                 $max_lb_drop
 * @property int                 $ability_slots
 * @property mixed               $magic_affinity
 * @property mixed               $element_resist
 * @property mixed               $status_resist
 * @property int                 $physical_resist
 * @property int                 $magical_resist
 * @property int                 $awakening_gil
 * @property mixed               $awakening_materials
 * @property mixed               $stings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereAbilitySlots($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereAttackCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereAttackFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereAwakeningGil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereAwakeningMaterials($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereEffectFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereElementResist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereGrowthPattern($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereLimitburstId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereMagicAffinity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereMagicalResist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereMaxLbDrop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry wherePhysicalResist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereRarity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereStats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereStatusResist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereStings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class UnitEntry extends \Eloquent {}
}

