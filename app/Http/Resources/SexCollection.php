<?php

namespace App\Http\Resources;

use App\Sex;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SexCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set sex id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Sex $sex) {
			unset($sex->id);
		});

		return $this->collection->toArray();
    }
}
