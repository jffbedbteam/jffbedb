<?php

namespace App\Http\Resources;

use App\Region;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RegionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set regions id as object key
        $this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Region $region) {
			unset($region->id);
		});

		return $this->collection->toArray();
    }
}
