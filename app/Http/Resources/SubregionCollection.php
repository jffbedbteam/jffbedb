<?php

namespace App\Http\Resources;

use App\Subregion;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SubregionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set subregions id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Subregion $subregion) {
			unset($subregion->id);
		});

		return $this->collection->toArray();
    }
}
