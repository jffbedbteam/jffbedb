<?php

namespace App\Http\Resources;

use App\UnitEntry;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UnitEntryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set unitentry id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (UnitEntry $unitEntry) {
			unset($unitEntry->id);
		});

		return $this->collection->toArray();
    }
}
