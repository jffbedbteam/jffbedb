<?php

namespace App\Http\Resources;

use App\Expedition;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ExpeditionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set expedition id as object key
		$this->collection=$this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Expedition $expedition) {
			unset($expedition->id);
		});

		return $this->collection->toArray();
    }
}
