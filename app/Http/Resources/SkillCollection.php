<?php

namespace App\Http\Resources;

use App\Skill;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SkillCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Skill $skill) {
			unset($skill->id);
		});

		return $this->collection->toArray();
    }
}
