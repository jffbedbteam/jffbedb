<?php

namespace App\Http\Resources;

use App\Enhancement;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EnhancementCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set enhancement id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Enhancement $enhancement) {
			unset($enhancement->id);
		});

		return $this->collection->toArray();
    }
}
