<?php

namespace App\Http\Resources;

use App\SummonBoard;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SummonBoardCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set summonboard id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (SummonBoard $summonBoard) {
			unset($summonBoard->id);
		});

		return $this->collection->toArray();
    }
}
