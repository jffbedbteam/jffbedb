<?php

namespace App\Http\Resources;

use App\Limitburst;
use Illuminate\Http\Resources\Json\ResourceCollection;

class LimitburstCollection extends ResourceCollection
{
	/**
	 * Transform the resource collection into an array.
	 *
	 * @param  \Illuminate\Http\Request
	 * @return array
	 */
	public function toArray($request)
	{
		//set limitburst id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Limitburst $limitburst) {
			unset($limitburst->id);
		});

		return $this->collection->toArray();
	}
}
