<?php

namespace App\Http\Resources;

use App\Item;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ItemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set Items id as object key
		$this->collection=$this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Item $item) {
			unset($item->id);
		});

		return $this->collection->toArray();
    }
}
