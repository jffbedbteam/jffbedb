<?php

namespace App\Http\Resources;

use App\Mission;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MissionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set missions id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Mission $mission) {
			unset($mission->id, $mission->dungeon_id);//unnecesary id's, already within properties objects
		});

		return $this->collection->toArray();
    }
}
