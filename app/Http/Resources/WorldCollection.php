<?php

namespace App\Http\Resources;

use App\World;
use Illuminate\Http\Resources\Json\ResourceCollection;

class WorldCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set units id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map( function(World $world){
			unset ($world->id);
		});

		return $this->collection->toArray();
    }
}
