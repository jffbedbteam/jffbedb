<?php

namespace App\Http\Resources;

use App\EquipmentSlot;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EquipmentSlotCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set equipmentslot id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (EquipmentSlot $equipmentSlot) {
			unset($equipmentSlot->id);
		});

		return $this->collection->toArray();
    }
}
