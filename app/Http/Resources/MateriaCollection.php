<?php

namespace App\Http\Resources;

use App\Materia;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MateriaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set materias id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Materia $materia) {
			unset($materia->id);
		});

		return $this->collection->toArray();
    }
}
