<?php

namespace App\Http\Resources;

use App\Summon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SummonCollection extends ResourceCollection
{
	/**
	 * Transform the resource collection into an array.
	 *
	 * @param  \Illuminate\Http\Request
	 * @return array
	 */
	public function toArray($request)
	{
		//set summons id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Summon $summon) {
			unset($summon->id);
		});

		return $this->collection->toArray();
	}
}
