<?php

namespace App\Http\Resources;

use App\Equipment;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EquipmentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set equipment id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Equipment $equipment) {
			unset($equipment->id); //unnecesary id's, already within properties objects
		});

		return $this->collection->toArray();
    }
}
