<?php

namespace App\Http\Resources;

use App\Town;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TownCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set units id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map( function(Town $town){
			unset ($town->id);
		});

		return $this->collection->toArray();
    }
}
