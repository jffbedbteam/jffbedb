<?php

namespace App\Http\Resources;

use App\Recipe;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RecipeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set recipes id as object key
    	$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Recipe $recipe) {
			unset($recipe->id);
		});

		return $this->collection->toArray();
	}
}
