<?php

namespace App\Http\Resources;

use App\EquipmentType;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EquipmentTypeCollection extends ResourceCollection
{
	/**
	 * Transform the resource collection into an array.
	 *
	 * @param  \Illuminate\Http\Request
	 * @return array
	 */
	public function toArray($request)
	{
		//set EquipmentType id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (EquipmentType $equipmentType) {
			unset($equipmentType->id);
		});

		return $this->collection->toArray();
	}
}
