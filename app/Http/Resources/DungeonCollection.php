<?php

namespace App\Http\Resources;

use App\Dungeon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class DungeonCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set dungeons id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Dungeon $dungeon) {
			unset($dungeon->id);
		});

		return $this->collection->toArray();
    }
}
