<?php

namespace App\Http\Resources;

use App\Mogking;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MogkingCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
		//set mogking id as object key
		$this->collection = $this->collection->keyBy('id');

		//remove id from object properties
		$this->collection->map(function (Mogking $mogking) {
			unset($mogking->id);
		});

		return $this->collection->toArray();
    }
}
