<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class APIResponseContentType
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		/**
		 * comply json:api specification:
		 * 		Servers MUST respond with a 415 Unsupported Media Type status code if
		 * 		a request specifies the header Content-Type: application/vnd.api+json with any media type parameters.
		 */
		if ($request->header('Content-Type') <> 'application/vnd.api+json') {
			return Response('Unsupported Media Type', Response::HTTP_UNSUPPORTED_MEDIA_TYPE, ['Content-Type' => 'application/vnd.api+json']);
		}

		$response = $next($request);

		/**
		 * comply json:api specification:
		 *        "Servers MUST send all JSON API data in response documents
		 *        with the header Content-Type: application/vnd.api+json
		 *        without any media type parameters."
		 *
		 * source: http://jsonapi.org/format/#content-negotiation-servers
		 */
		$response->header('Content-Type', 'application/vnd.api+json');

		return $response;
	}
}
