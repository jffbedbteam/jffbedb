<?php

namespace App\Http\Controllers;

use App\Http\Resources\UnitCollection;
use App\Http\Resources\UnitResource;
use App\Skill;
use App\Unit;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class UnitController
 *
 * @package App\Http\Controllers
 */
class UnitController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index(Request $request)
	{
		try {
			//if querystring contains 'page' (for pagination)
			if ($request->has('page') || $request->has('per_page')) {
				//grab 'per_page' parameter, or use 10 as default
				$per_page = $request->query('per_page', 10);

				//return paginated units persisting(appending) query string 'per_page' parameter.
				$units = Unit::with(['entries', 'sex', 'game', 'job'])
							 ->paginate($per_page)
							 ->appends('per_page', $per_page);
			} else {
				$units = Unit::with(['entries', 'sex', 'game', 'job'])->get();
			}


			$units->map(function (Unit $unit) {
				//unset unnecesary id's, already within properties objects
				unset($unit->game_id, $unit->job_id, $unit->sex_id);

				$this->setEntriesIdKey($unit);
				$this->setSkillsIdKey($unit);
			});

			return (new UnitCollection($units))->response(Response::HTTP_OK);
		} catch (Exception $e) {
			return Response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show($id)
	{
		try {
			$unit = Unit::with(['entries', 'sex', 'game', 'job'])
						->findOrFail($id);

			unset($unit->game_id, $unit->job_id, $unit->sex_id); //unnecesary id's, already within properties objects

			$this->setEntriesIdKey($unit);
			$this->setSkillsIdKey($unit);

			return (new UnitResource($unit))->Response(Response::HTTP_OK);
		} catch (Exception $e) {
			return Response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}


	/**
	 * Lists all units with id, job_id, names, rarity_max, keyed by id including job relationship
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function list(Request $request)
	{
		try {
			if ($request->has('page') || $request->has('per_page')) {
				//grab 'per_page' parameter, or use 10 as default
				$per_page = $request->query('per_page', 10);

				//return paginated units persisting(appending) query string 'per_page' parameter.
				$units = Unit::select(['id', 'job_id', 'names', 'rarity_min', 'rarity_max'])
							 ->with('job:id,name')//Eager Loading Specific Columns
							 ->paginate($per_page)
							 ->appends('per_page', $per_page);
			} else {
				$units = Unit::select(['id', 'job_id', 'names', 'rarity_min', 'rarity_max'])
							 ->with('job:id,name')//Eager Loading Specific Columns
							 ->get();
			}


			//unset the job_id property for all units, already in job property object
			$units->map(function (Unit $unit) {
				unset($unit->job_id);
			});


			return (new UnitCollection($units))->response(Response::HTTP_OK);
		} catch (Exception $e) {
			return Response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * Display unit by name
	 *
	 * @param  string $name
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function showByName($name)
	{
		try {
			$unit = Unit::with(['entries', 'sex', 'game', 'job'])
						->where('name', $name)
						->firstOrFail();

			unset($unit->game_id, $unit->job_id, $unit->sex_id); //unnecesary id's, already within properties objects
			$this->setEntriesIdKey($unit);
			$this->setSkillsIdKey($unit);

			return (new UnitResource($unit))->Response(Response::HTTP_OK);
		} catch (Exception $e) {
			return Response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * * receives a $unit and sets entries id's as object keys
	 *
	 * @param $unit
	 *
	 * @return void
	 */
	protected function setEntriesIdKey(Unit &$unit)
	{
		try {
			if (!is_null($unit->entries)) {
				$unit->setRelation('entries', $unit->entries->keyBy('id')); //key entries by id
				$unit->entries->map(function ($entry, $entry_id) use ($unit) {
					unset($entry->id); //remove id from entry properties (key has the id now)
					unset($entry->unit_id);
				});
			}
		} catch (Exception $e) {
			dd($e);
		}

	}


	/**
	 * receives a $unit and sets skills id's as object keys
	 *
	 * @param $unit
	 *
	 * @return void
	 */
	protected function setSkillsIdKey(Unit &$unit)
	{
		try {
			if (!is_null($unit->skills)) {
				$unit->skills = $unit->skills->keyBy('id');

				$skills = collect($unit->skills);

				$skills->transform(function ($skill) {
					unset($skill['id']); //remove id from skill properties (key has the id now)
					return $skill;
				});

				$unit->skills = $skills;
			}

		} catch (Exception $e) {
			dd($e);
		}
	}

}
