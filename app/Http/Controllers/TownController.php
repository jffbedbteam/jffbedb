<?php

namespace App\Http\Controllers;

use App\Http\Resources\JobCollection;
use App\Http\Resources\TownCollection;
use App\Http\Resources\TownResource;
use App\Town;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class TownController
 * @package App\Http\Controllers
 */
class TownController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index(Request $request)
	{
		try {
			//if querystring contains 'page' or 'per_page (for pagination)
			if ($request->has('page') || $request->has('per_page')) {
				//grab 'per_page' parameter, or use 10 as default
				$per_page = $request->query('per_page', 10);

				//return paginated subregions persisting(appending) query string 'per_page' parameter.
				$towns = Town::paginate($per_page)
							 ->appends('per_page', $per_page);
			} else {
				$towns = Town::all();
			}

			return (new TownCollection($towns))->response(Response::HTTP_OK);
		} catch (Exception $e) {
			return Response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show($id)
	{
		try {
			$town = Town::findOrFail($id);

			return (new TownResource($town))->response(Response::HTTP_OK);
		} catch (Exception $e) {
			return Response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
