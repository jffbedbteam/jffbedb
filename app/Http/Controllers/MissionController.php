<?php

namespace App\Http\Controllers;

use App\Http\Resources\MissionCollection;
use App\Http\Resources\MissionResource;
use App\Mission;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class MissionController
 *
 * @package App\Http\Controllers
 */
class MissionController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index(Request $request)
	{
		try {
			//if querystring contains 'page' or 'per_page (for pagination)
			if ($request->has('page') || $request->has('per_page')) {
				//grab 'per_page' parameter, or use 10 as default
				$per_page = $request->query('per_page', 10);

				//return paginated missions persisting(appending) query string 'per_page' parameter.
				$missions = Mission::with('dungeon')
								   ->paginate($per_page)
								   ->appends('per_page', $per_page);
			} else {
				//return all missions with id as object key
				$missions = Mission::with('dungeon')
								   ->get();
			}


			return (new MissionCollection($missions))->response(Response::HTTP_OK);
		} catch (Exception $e) {
			return Response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show($id)
	{
		try {
			$mission = Mission::with('dungeon')
							  ->findOrFail($id);

			unset($mission->dungeon_id);//unnecesary id's, already within properties objects

			return (new MissionResource($mission))->response(Response::HTTP_OK);
		} catch (Exception $e) {
			return Response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
