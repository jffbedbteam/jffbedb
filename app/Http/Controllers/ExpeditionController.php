<?php

namespace App\Http\Controllers;

use App\Expedition;
use App\Http\Resources\ExpeditionCollection;
use App\Http\Resources\ExpeditionResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class ExpeditionController
 *
 * @package App\Http\Controllers
 */
class ExpeditionController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index(Request $request)
	{
		try {
			//if querystring contains 'page' or 'per_page (for pagination)
			if ($request->has('page') || $request->has('per_page')) {
				//grab 'per_page' parameter, or use 10 as default
				$per_page = $request->query('per_page', 10);

				//return paginated expeditions persisting(appending) query string 'per_page' parameter.
				$expeditions = Expedition::paginate($per_page)
										 ->appends('per_page', $per_page);
			} else {
				$expeditions = Expedition::all();
			}

			return (new ExpeditionCollection($expeditions))->response(Response::HTTP_OK);
		} catch (Exception $e) {
			return Response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show($id)
	{
		try {
			$expedition = Expedition::findOrFail($id);

			return (new ExpeditionResource($expedition))->response(Response::HTTP_OK);
		} catch (Exception $e) {
			return Response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
