<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Subregion
 *
 * @property int                                                          $id
 * @property string                                                       $name
 * @property \Carbon\Carbon|null                                          $created_at
 * @property \Carbon\Carbon|null                                          $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subregion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subregion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subregion whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subregion whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Dungeon[] $dungeons
 * @property-read \App\Region                                             $region
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Town[] $towns
 * @property mixed $names
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subregion whereNames($value)
 */
class Subregion extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts =[
		'names' => 'collection'
	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function region()
	{
		return $this->belongsTo(Region::class);
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function dungeons()
	{
		return $this->hasMany(Dungeon::class);
	}

	public function towns()
	{
		return $this->hasMany(Town::class);
	}
}
