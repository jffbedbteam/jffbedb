<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Limitburst
 *
 * @property int                 $id
 * @property string              $name
 * @property int                 $cost
 * @property mixed               $attack_count
 * @property mixed               $attack_frames
 * @property mixed               $effect_frames
 * @property int                 $move_type
 * @property string              $damage_type
 * @property mixed               $element_inflict
 * @property int                 $levels
 * @property mixed               $min_level
 * @property mixed               $max_level
 * @property mixed               $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereAttackCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereAttackFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereDamageType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereEffectFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereElementInflict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereLevels($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereMaxLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereMinLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereMoveType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Limitburst whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Limitburst extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'min_level'       => 'collection',
		'max_level'       => 'collection',
		'element_inflict' => 'collection',
		'strings'         => 'collection',
	];

	/**
	 *
	 */
	const DAMAGE_TYPE = [
		'Physical' => 'Physical',
		'Magic'    => 'Magic',
		'Hybrid'   => 'Hybrid',
		'None'     => 'None',
	];

	/**
	 *
	 */
	const ELEMENTS = [
		"Fire"      => "Fire",
		"Ice"       => "Ice",
		"Lightning" => "Lightning",
		"Water"     => "Water",
		"Wind"      => "Wind",
		"Earth"     => "Earth",
		"Light"     => "Light",
		"Dark"      => "Dark",
	];

}
