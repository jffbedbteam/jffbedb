<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Mission
 *
 * @property int                 $id
 * @property int                 $dungeon_id
 * @property string              $name
 * @property string              $type
 * @property int                 $wave_count
 * @property string              $cost_type
 * @property int                 $cost
 * @property mixed               $flags
 * @property mixed               $reward
 * @property int                 $gil
 * @property int                 $exp
 * @property mixed               $challenges
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereChallenges($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereCostType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereDungeonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereFlags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereGil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereWaveCount($value)
 * @mixin \Eloquent
 * @property-read \App\Dungeon   $dungeon
 */
class Mission extends Model
{
	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

	/**
	 * The attributes that should be hidden for serialization.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at'];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'flags'      => 'collection',
		'reward'     => 'collection',
		'challenges' => 'collection',
	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function dungeon()
	{
		return $this->belongsTo(Dungeon::class);
	}
}
