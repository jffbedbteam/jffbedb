<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Summon
 *
 * @property int                                                              $id
 * @property mixed                                                            $names
 * @property string                                                           $image
 * @property string                                                           $icon
 * @property mixed                                                            $skill
 * @property mixed                                                            $color
 * @property mixed                                                            $entries
 * @property \Carbon\Carbon|null                                              $created_at
 * @property \Carbon\Carbon|null                                              $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereEntries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereSkill($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Summon whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SummonBoard[] $summonBoard
 */
class Summon extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'names'   => 'collection',
		'skill'   => 'collection',
		'color'   => 'collection',
		'entries' => 'collection',
	];

	/**
	 *
	 */
	const COLOR_AFFINITY = [
		'Blue'   => 'Blue',
		'Green'  => 'Green',
		'Red'    => 'Red',
		'Violet' => 'Violet',
		'White'  => 'White',
		'Black'  => 'Black',
		'Orange' => 'Orange',
		'Yellow' => 'Yellow',
	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function summonBoard()
	{
		return $this->hasMany(SummonBoard::class);
	}
}
