<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Unit
 *
 * @property int                                                            $id
 * @property int                                                            $rarity_min
 * @property int                                                            $rarity_max
 * @property string                                                         $name
 * @property mixed                                                          $names
 * @property int                                                            $game_id
 * @property int                                                            $job_id
 * @property int                                                            $sex_id
 * @property int                                                            $tribe_id
 * @property int                                                            $is_summonable
 * @property string                                                         $TMR_type
 * @property int                                                            $TMR_id
 * @property mixed                                                          $equip
 * @property mixed                                                          $skills
 * @property \Carbon\Carbon|null                                            $created_at
 * @property \Carbon\Carbon|null                                            $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereEquip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereGame($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereIsSummonable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereJobId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereRarityMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereRarityMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereSexId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereSkills($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereTMRId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereTMRType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereTribeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Unit whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UnitEntry[] $entries
 * @property-read \App\Game                                                 $game
 * @property-read \App\Job                                                  $job
 * @property-read \App\Sex                                                  $sex
 */
class Unit extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'names'  => 'collection',
		'skills' => 'collection',
	];


	/**
	 *
	 */
	const UNIT_RARITY = [
		'1 stars' => '1',
		'2 stars' => '2',
		'3 stars' => '3',
		'4 stars' => '4',
		'5 stars' => '5',
		'6 stars' => '6',
		//'7 stars' => '7', To be released soon in Japanese version
	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function game()
	{
		return $this->belongsTo(Game::class);
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function job()
	{
		return $this->belongsTo(Job::class);
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function sex()
	{
		return $this->belongsTo(Sex::class);
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function entries()
	{
		return $this->hasMany(UnitEntry::class);
	}
}
