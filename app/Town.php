<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Town
 *
 * @property int $id
 * @property mixed $names
 * @property int $world_id
 * @property int $region_id
 * @property int $subregion_id
 * @property mixed $position
 * @property string $icon
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Subregion $subregion
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereSubregionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereWorldId($value)
 * @mixin \Eloquent
 */
class Town extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'names'  => 'collection',
		'position' => 'collection',
	];

	public function subregion()
	{
		return $this->belongsTo(Subregion::class);
	}
}
