<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UnitEntry
 *
 * @property int                 $id
 * @property int                 $unit_id
 * @property int                 $compendium_id
 * @property int                 $rarity
 * @property int                 $growth_pattern
 * @property mixed               $stats
 * @property int                 $limitburst_id
 * @property int                 $attack_count
 * @property mixed               $attack_frames
 * @property mixed               $effect_frames
 * @property int                 $max_lb_drop
 * @property int                 $ability_slots
 * @property mixed               $magic_affinity
 * @property mixed               $element_resist
 * @property mixed               $status_resist
 * @property int                 $physical_resist
 * @property int                 $magical_resist
 * @property int                 $awakening_gil
 * @property mixed               $awakening_materials
 * @property mixed               $stings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereAbilitySlots($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereAttackCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereAttackFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereAwakeningGil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereAwakeningMaterials($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereEffectFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereElementResist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereGrowthPattern($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereLimitburstId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereMagicAffinity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereMagicalResist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereMaxLbDrop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry wherePhysicalResist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereRarity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereStats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereStatusResist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereStings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property mixed               $strings
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UnitEntry whereStrings($value)
 */
class UnitEntry extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'stats'               => 'collection',
		'awakening_materials' => 'collection',
		'strings'             => 'collection',
	];


	/**
	 *
	 */
	public function unit()
	{
		$this->belongsTo(Unit::class);
	}
}
