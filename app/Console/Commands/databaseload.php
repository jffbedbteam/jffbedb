<?php

namespace App\Console\Commands;

use App\Jobs\LoadFFBEjsonDatabase;
use Carbon\Carbon;
use Illuminate\Console\Command;

class databaseload extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'database:load';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Loads database repositories and runs ETL';


	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->warn(Carbon::now() . ': This proccess can take from ~2 to ~15 minutes, please be patient...');
		LoadFFBEjsonDatabase::dispatch();
		$this->info(Carbon::now() . ': Repositories and Database Loaded Succesfully');
	}
}
