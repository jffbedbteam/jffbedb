<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Expedition
 *
 * @property int                 $id
 * @property string              $name
 * @property int                 $type
 * @property int                 $cost
 * @property string              $rank
 * @property int                 $difficulty
 * @property int                 $duration
 * @property int                 $units
 * @property mixed               $required
 * @property int                 $next_id
 * @property mixed               $reward
 * @property int                 $relics
 * @property mixed               $exp_levels
 * @property mixed               $unit_bonus
 * @property int                 $unit_bonus_max
 * @property mixed               $item_bonus
 * @property mixed               $stat_bonus
 * @property mixed               $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereDifficulty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereExpLevels($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereItemBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereNextId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereRelics($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereStatBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereUnitBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereUnitBonusMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereUnits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Expedition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Expedition extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'required'   => 'collection',
		'reward'     => 'collection',
		'exp_levels' => 'collection',
		'unit_bonus' => 'collection',
		'item_bonus' => 'collection',
		'stat_bonus' => 'collection',
		'strings'    => 'collection',
	];
}
