<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\World
 *
 * @property int $id
 * @property mixed $names
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Region[] $regions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\World whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\World whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\World whereNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\World whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class World extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'names'  => 'collection',
	];



	public function regions()
	{
		return $this->hasMany(Region::class);
	}
}
