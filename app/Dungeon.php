<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Dungeon
 *
 * @property int                                                          $id
 * @property string                                                       $name
 * @property int                                                          $region_id
 * @property int                                                          $subregion_id
 * @property string                                                       $type
 * @property mixed                                                        $position
 * @property string                                                       $icon_id
 * @property \Carbon\Carbon|null                                          $created_at
 * @property \Carbon\Carbon|null                                          $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereIconId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereSubregionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Mission[] $missions
 * @property-read \App\Subregion                                          $subregion
 * @property-read \App\Region|null $region
 * @property mixed|null $names
 * @property string|null $icon
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Dungeon whereNames($value)
 */
class Dungeon extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'names' => 'collection',
		'position' => 'collection',
	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function subregion()
	{
		return $this->belongsTo(Subregion::class);
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function missions()
	{
		return $this->hasMany(Mission::class);
	}
}
