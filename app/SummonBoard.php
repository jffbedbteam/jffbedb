<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SummonBoard
 *
 * @property int                 $id
 * @property int                 $summon_id
 * @property mixed               $node
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SummonBoard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SummonBoard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SummonBoard whereNode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SummonBoard whereSummonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SummonBoard whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Summon    $summon
 */
class SummonBoard extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = ['node' => 'collection'];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function summon()
	{
		return $this->belongsTo(Summon::class);
	}
}
