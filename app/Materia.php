<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Materia
 *
 * @property int                 $id
 * @property string              $name
 * @property int                 $compendium_id
 * @property mixed               $skills
 * @property mixed               $effects
 * @property int                 $unique
 * @property int                 $price_buy
 * @property int                 $price_sell
 * @property string              $icon
 * @property mixed               $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereEffects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia wherePriceBuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia wherePriceSell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereSkills($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereUnique($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Materia whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Materia extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'skills'  => 'collection',
		'effects' => 'collection',
		'strings' => 'collection',
	];
}
