<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Skill
 *
 * @property int                 $id
 * @property string              $type
 * @property int                 $active
 * @property int                 $usable_in_exploration
 * @property string              $name
 * @property int                 $compendium_id
 * @property int                 $rarity
 * @property string              $magic_type
 * @property int                 $mp_cost
 * @property int                 $is_sealable
 * @property int                 $is_reflectable
 * @property mixed               $attack_count
 * @property mixed               $attack_frames
 * @property mixed               $effect_frames
 * @property int                 $move_type
 * @property mixed               $effect_type
 * @property string              $attack_type
 * @property mixed               $element_inflict
 * @property mixed               $effects
 * @property mixed               $effects_raw
 * @property mixed               $unit_restriction
 * @property string              $icon
 * @property mixed               $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereAttackCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereAttackFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereAttackType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereEffectFrames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereEffectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereEffects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereEffectsRaw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereElementInflict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereIsReflectable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereIsSealable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereMagicType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereMoveType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereMpCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereRarity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereUnitRestriction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereUsableInExploration($value)
 * @mixin \Eloquent
 */
class Skill extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'effect_type'      => 'collection',
		'element_inflict'  => 'collection',
		'effects'          => 'collection',
		'unit_restriction' => 'collection',
		'strings'          => 'collection',
	];


	/**
	 *
	 */
	const SKILL_TYPE = [
		'Magic'   => 'MAGIC',
		'Ability' => 'ABILITY',
	];


	/**
	 *
	 */
	const SKILL_RARITY = [
		'1 Stars' => 1,
		'2 Stars' => 2,
		'3 Stars' => 3,
		'4 Stars' => 4,
		'5 Stars' => 5,
		'6 Stars' => 6,
		'7 Stars' => 7,
		'8 Stars' => 8,
	];


	/**
	 *
	 */
	const MAGIC_TYPE = [
		'White' => 'WHITE',
	];


	/**
	 *
	 */
	const EFFECT_TYPE = [
		'Default'        => 'Default',
		'Revive'         => 'Revive',
		'Percentual DMG' => 'Percentual DMG',
		'Drain HP'       => 'Drain HP',
		'Drain MP'       => 'Drain MP',
		'Instant KO'     => 'Instant KO',
		'Steal Item'     => 'Steal Item',
		'Steal Gold'     => 'Steal Gold',
	];


	/**
	 *
	 */
	const ATTACK_TYPE = [
		'Physical' => 'Physical',
		'Magic'    => 'Magic',
		'Hybrid'   => 'Hybrid',
		'None'     => 'None',
		'Unknown'  => 'Unknown',
	];


}
