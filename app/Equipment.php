<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Equipment
 *
 * @property int                     $id
 * @property string                  $name
 * @property int                     $compendium_id
 * @property int                     $compendium_shown
 * @property int                     $rarity
 * @property int                     $type_id
 * @property int                     $slot_id
 * @property int                     $is_twohanded
 * @property mixed                   $dmg_variance
 * @property int                     $accuracy
 * @property string                  $requirement_type
 * @property int                     $requirement_id
 * @property mixed                   $skills
 * @property mixed                   $effects
 * @property mixed                   $stats
 * @property int                     $price_buy
 * @property int                     $price_sell
 * @property string                  $icon
 * @property mixed                   $strings
 * @property \Carbon\Carbon|null     $created_at
 * @property \Carbon\Carbon|null     $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereAccuracy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereCompendiumShown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereDmgVariance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereEffects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereIsTwohanded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment wherePriceBuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment wherePriceSell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereRarity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereRequirementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereRequirementType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereSkills($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereSlotId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereStats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Equipment whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\EquipmentSlot $slot
 * @property-read \App\EquipmentType $type
 */
class Equipment extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'skills'  => 'collection',
		'effects' => 'collection',
		'stats'   => 'collection',
		'strings' => 'collection',
	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function type()
	{
		return $this->belongsTo(EquipmentType::class);
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function slot()
	{
		return $this->belongsTo(EquipmentSlot::class);
	}
}
