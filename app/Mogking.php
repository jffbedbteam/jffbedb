<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Mogking
 *
 * @mixin \Eloquent
 * @property int                 $id
 * @property string              $currency_item
 * @property string              $reward_name
 * @property string              $reward_type
 * @property int                 $reward_id
 * @property int                 $currency_price
 * @property int                 $amount
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mogking whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mogking whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mogking whereCurrencyItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mogking whereCurrencyPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mogking whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mogking whereRewardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mogking whereRewardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mogking whereRewardType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mogking whereUpdatedAt($value)
 */
class Mogking extends Model
{
	//set table name as it should not be plural
	protected $table = 'mogking';

	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	/**
	 *
	 */
	const REWARD_TYPE = [
		"ITEM"    => "ITEM",
		"MATERIA" => "MATERIA",
		"EQUIP"   => "EQUIP",
		"UNIT"    => "UNIT",
		"RECIPE"  => "RECIPE",
		"KEYITEM" => "KEYITEM",
	];
}
