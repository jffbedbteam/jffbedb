<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Sex
 *
 * @property int                                                       $id
 * @property string                                                    $name
 * @property \Carbon\Carbon|null                                       $created_at
 * @property \Carbon\Carbon|null                                       $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Unit[] $units
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Sex extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];


	/**
	 *
	 */
	const SEX = [
		'Other'  => '0',
		'Male'   => '1',
		'Female' => '2',
	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function units()
	{
		return $this->hasMany(Unit::class);
	}
}
