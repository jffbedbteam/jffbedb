<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Job
 *
 * @property int                                                       $id
 * @property string                                                    $name
 * @property \Carbon\Carbon|null                                       $created_at
 * @property \Carbon\Carbon|null                                       $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Unit[] $units
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Job extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];


	/**
	 *
	 */
	const JOB = [
		'Knight'             => 1,
		'Dragoon'            => 2,
		'Paladin'            => 3,
		'Chaos Knight'       => 4,
		'Warrior'            => 5,
		'Ranger'             => 6,
		'Adventurer'         => 7,
		'Pirate'             => 8,
		'Monk'               => 9,
		'Thief'              => 10,
		'Bandit'             => 11,
		'Samurai'            => 12,
		'Assassin'           => 13,
		'Black Mage'         => 14,
		'Evoker'             => 18,
		'Mage'               => 19,
		'Dark Mage'          => 20,
		'White Mage'         => 22,
		'Green Mage'         => 24,
		'High Priest'        => 25,
		'Spellblade'         => 27,
		'Warmage'            => 28,
		'Magic Warrior'      => 29,
		'Red Mage'           => 31,
		'Gunner'             => 32,
		'Sky Pirate'         => 33,
		'Imperial'           => 34,
		'Machinist'          => 35,
		'Songstress'         => 36,
		'Bard'               => 37,
		'Dancer'             => 38,
		'Scholar'            => 39,
		'Salve-maker'        => 40,
		'Professor'          => 41,
		'Reaper'             => 42,
		'Ravager'            => 43,
		'Black Belt'         => 44,
		'Viking'             => 45,
		'Gladiator'          => 46,
		'Ninja'              => 47,
		'Rune Knight'        => 49,
		'General'            => 50,
		'Archmage'           => 51,
		'Lancer'             => 52,
		'Warrior of Light'   => 53,
		'Sage'               => 54,
		'Princess'           => 55,
		'Emperor'            => 56,
		'Airship Tech'       => 57,
		'Summoner'           => 58,
		'Goddess'            => 59,
		'l\'Cie'             => 60,
		'Squire'             => 61,
		'Holy Knight'        => 62,
		'Cleric'             => 63,
		'Fell Knight'        => 64,
		'Engineer'           => 65,
		'Juggler'            => 66,
		'Fencer'             => 67,
		'Dark Knight'        => 68,
		'Agito Cadet'        => 69,
		'Tactician'          => 70,
		'White Wizard'       => 71,
		'Red Wizard'         => 72,
		'Black Wizard'       => 73,
		'Holy Sword Hero'    => 74,
		'Young Girl'         => 75,
		'Sprite'             => 76,
		'Guard Captain'      => 78,
		'Dark Spirit'        => 79,
		'Water Priestess'    => 80,
		'Soldier'            => 81,
		'Mercenary'          => 82,
		'Holy Swordsman'     => 83,
		'Spy'                => 85,
		'Crown Prince'       => 86,
		'Dragon King'        => 87,
		'Serpent'            => 88,
		'Kandar'             => 89,
		'Killer Machine'     => 90,
		'Liquid Metal Slime' => 91,
		'Golem'              => 92,
		'Slime'              => 93,
		'Berserker'          => 94,
		'Guardian'           => 95,
		'Magitek Soldier'    => 96,
		'Swordsman'          => 97,
		'God of Creation'    => 98,
		'Scion'              => 99,
		'Gambler'            => 100,
		'Wildling'           => 101,
		'Prince'             => 102,
		'Maid'               => 103,
		'Chocobo Knight'     => 104,
		'Archer'             => 105,
		'Blacksmith'         => 106,
		'Divine Knight'      => 107,
		'Astrologist'        => 108,
		'Veritas'            => 109,
		'Moogle'             => 110,
		'Onion Knight'       => 111,
		'Hunter'             => 112,
		'Kingsglaive'        => 113,
		'Royal Vagrant'      => 116,
		'Masked Wonder'      => 117,
		'SOLDIER'            => 118,
		'Costume Girl'       => 122,
		'Mog Friend'         => 125,
		'Flowery Lady'       => 126,
		'Dream Mage'         => 128,
		'Tailor'             => 129,
		'King\'s Shield'     => 130,
		'Prince\'s Friend'   => 131,
		'Barrier Mage'       => 132,
		'Black Mage Warrior' => 133,
		'Chocobo Rider'      => 134,
		'YoRHa Troop'        => 135,
		'Machine Lifeform'   => 136,
		'Priest'             => 137,
		'Enhancer'           => 900,
		'EXP Unit'           => 901,
		'Gil Unit'           => 902,
		'Fusion Unit'        => 903,
		'Demon Knight'       => 9001,
		'Vampire'            => 9002,
		'White Witch'        => 9003,
		'Tech Mage'          => 9004,
		'White Knight'       => 9005,
		'Ariana Grande'      => 9006,
		'Cupid Archer'       => 9008,
		'Cannoneer'          => 9009,
		'Archadian Judge'    => 9010,
		'Artisan'            => 9011,
		'Ex-YoRHa Android'   => 9012,
		'Cheerleader'        => 9014,
		'Grim Lord'          => 9015,
		'Illusionist'        => 9016,
		'Marshal'            => 9017,
	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function units()
	{
		return $this->hasMany(Unit::class);
	}
}
