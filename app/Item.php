<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Item
 *
 * @property int                 $id
 * @property string              $name
 * @property string              $type
 * @property int                 $compendium_id
 * @property int                 $compendium_shown
 * @property int                 $usable_in_combat
 * @property int                 $usable_in_exploration
 * @property mixed               $flags
 * @property int                 $carry_limit
 * @property int                 $stack_size
 * @property int                 $price_buy
 * @property int                 $price_sell
 * @property mixed               $effects
 * @property mixed               $effects_raw
 * @property string              $icon
 * @property mixed               $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCarryLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCompendiumShown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereEffects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereEffectsRaw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereFlags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item wherePriceBuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item wherePriceSell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereStackSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUsableInCombat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUsableInExploration($value)
 * @mixin \Eloquent
 */
class Item extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		//'flags' => 'collection',
		'effects' => 'collection',
		//'effects_raw' => 'collection',
		'strings' => 'collection',
	];

	/**
	 *
	 */
	const ITEM_TYPE = [
		'Consumable' => 'Consumable',
		'Awakening'  => 'Awakening',
		'4'          => '4',
		'Item'       => 'Item',
	];


}
