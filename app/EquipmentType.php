<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\EquipmentType
 *
 * @property int                 $id
 * @property string              $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentType whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Equipment $equipment
 */
class EquipmentType extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	/**
	 *
	 */
	const EQUIPMENT_TYPE = [
		'Dagger'       => '1',
		'Sword'        => '2',
		'Great Sword'  => '3',
		'Katana'       => '4',
		'Staff'        => '5',
		'Rod'          => '6',
		'Bow'          => '7',
		'Axe'          => '8',
		'Hammer'       => '9',
		'Spear'        => '10',
		'Harp'         => '11',
		'Whip'         => '12',
		'Throwing'     => '13',
		'Gun'          => '14',
		'Mace'         => '15',
		'Fist'         => '16',
		'Light Shield' => '30',
		'Heavy Shield' => '31',
		'Hat'          => '40',
		'Helm'         => '41',
		'Cloth'        => '50',
		'Light Armor'  => '51',
		'Heavy Armor'  => '52',
		'Robe'         => '53',
		'Accesory'     => '60',
	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function equipment()
	{
		return $this->hasMany(Equipment::class);
	}
}
