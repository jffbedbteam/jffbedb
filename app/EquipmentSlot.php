<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\EquipmentSlot
 *
 * @property int                 $id
 * @property string              $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentSlot whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentSlot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentSlot whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EquipmentSlot whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Equipment $equipment
 */
class EquipmentSlot extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function equipment()
	{
		return $this->hasMany(Equipment::class);
	}
}
