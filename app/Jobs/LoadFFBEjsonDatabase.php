<?php

namespace App\Jobs;

use App\{
	Dungeon, Enhancement, Equipment, EquipmentSlot, EquipmentType, Expedition, Game, Item, Job, Limitburst, Materia, Mission, Mogking, Recipe, Region, Sex, Skill, Subregion, Summon, Town, Unit, UnitEntry, World
};
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Psy\Exception\ErrorException;


/**
 * Class LoadFFBEjsonDatabase
 *
 * @package App\Jobs
 */
class LoadFFBEjsonDatabase implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * @var string
	 */
	private $ffbeLocalRepo;
	/**
	 * @var string
	 */
	private $ffbeSchemasLocalRepo;


	/**
	 * Create a new job instance.
	 */
	public function __construct()
	{
		$this->ffbeLocalRepo        = public_path('ffbe');
		$this->ffbeSchemasLocalRepo = public_path('ffbeSchemas');

	}


	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$this->checkFfbeRepositories();
		$this->fetchFfbeRepositories();
		$this->runETL();
	}


	/**
	 *  checks if database directory does not exist, if it doesn't, clone it from github
	 */
	private function checkFfbeRepositories()
	{
		chdir(public_path());

		if (!File::exists($this->ffbeLocalRepo)) {
			exec('git clone https://github.com/aEnigmatic/ffbe.git 2>&1', $commandOutput);

			//Log to storage/logs/laravel.log
			foreach ($commandOutput as $item) {
				Log::info('aEnigmatic/ffbe: ' . $item);
			}
			unset($commandOutput);
		}

		if (!File::exists($this->ffbeSchemasLocalRepo)) {
			exec('git clone https://github.com/Jhoalx/ffbeSchemas.git 2>&1', $commandOutput);

			//Log to storage/logs/laravel.log
			foreach ($commandOutput as $item) {
				Log::info('Jhoalx/ffbeSchemas: ' . $item);
			}
			unset($commandOutput);
		}
	}


	/**
	 * clone aEnigmatic/ffbe github repository to public/ffbe
	 */
	private function fetchFfbeRepositories()
	{
		chdir($this->ffbeLocalRepo);

		// store command execution output in $commandOutput
		// "2>&1" output redirection sends standard error to where ever standard output is being redirected
		exec('git fetch --all 2>&1', $commandOutput);
		exec('git reset --hard origin/master 2>&1', $commandOutput);

		//Log to storage/logs/laravel.log
		foreach ($commandOutput as $item) {
			Log::info('aEnigmatic/ffbe: ' . $item);
		}
		unset($commandOutput);

		chdir($this->ffbeSchemasLocalRepo);

		// store command execution output in $commandOutput
		// "2>&1" output redirection sends standard error to where ever standard output is being redirected
		exec('git fetch --all 2>&1', $commandOutput);
		exec('git reset --hard origin/master 2>&1', $commandOutput);

		//Log to storage/logs/laravel.log
		foreach ($commandOutput as $item) {
			Log::info('Jhoalx/ffbeSchemas: ' . $item);
		}
		unset($commandOutput);
	}


	/**
	 * Extract info from public/ffbe json files, transforms it and insert on Database
	 */
	private function runETL()
	{
		$output = new \Symfony\Component\Console\Output\ConsoleOutput();

		$output->writeln(Carbon::now() . ": Parsing units.json file");
		$this->parseUnitsFile();
		$output->writeln("<info>units.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing skills.json file");
		$this->parseSkillsFile();
		$output->writeln("<info>skills.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing equipment.json file");
		$this->parseEquipmentFile();
		$output->writeln("<info>equipment.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing worlds.json file");
		$this->parseWorldsFile();
		$output->writeln("<info>worlds.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing towns.json file");
		$this->parseTownsFile();
		$output->writeln("<info>towns.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing dungeons.json file");
		$this->parseDungeonsFile();
		$output->writeln("<info>dungeons.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing missions.json file");
		$this->parseMissionsFile();
		$output->writeln("<info>missions.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing items.json file");
		$this->parseItemsFile();
		$output->writeln("<info>items.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing limitbursts.json file");
		$this->parseLimitburstsFile();
		$output->writeln("<info>limitbursts.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing materia.json file");
		$this->parseMateriaFile();
		$output->writeln("<info>materia.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing enhancements.json file");
		$this->parseEnhancementsFile();
		$output->writeln("<info>enhancements.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing expeditions.json file");
		$this->parseExpeditionsFile();
		$output->writeln("<info>expeditions.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing mog_king.json file");
		$this->parseMogkingFile();
		$output->writeln("<info>mog_king.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing recipes.json file");
		$this->parseRecipesFile();
		$output->writeln("<info>recipes.json parsed</info>");

		$output->writeln(Carbon::now() . ": Parsing summons.json file");
		$this->parseSummonsFile();
		$output->writeln("<info>summons.json parsed</info>");

		$output->writeln('<warning>PENDING summons_boards.json</warning>');
	}


	/**
	 *
	 */
	private function parseUnitsFile()
	{
		$units = collect(json_decode(File::get(public_path('ffbe/units.json')), true));

		$sexes = collect();
		$jobs  = collect();
		$games = collect();


		/**
		 * iterates through the unit collection and get all games/collaborations,
		 * Sexes ands Jobs with it's ids
		 */
		$units->map(function ($unit) use ($games, $sexes, $jobs) {
			$games->put($unit['game_id'], $unit['game']);
			$sexes->put($unit['sex_id'], $unit['sex']);
			$jobs->put($unit['job_id'], $unit['job']);
		});


		/**
		 * Save games to database
		 */
		$games->map(function ($game, $key) {
			$newGame       = Game::firstOrNew(['id' => $key]);
			$newGame->name = $game;
			$newGame->save();
		});


		/**
		 * Save sexes to database
		 */
		$sexes->map(function ($sex, $key) {
			Sex::firstOrCreate(['id' => $key, 'name' => $sex]);
		});


		/**
		 * Save Jobs to database
		 */
		$jobs->map(function ($job, $key) {
			Job::firstOrCreate(['id' => $key, 'name' => $job]);
		});


		/**
		 * Save units and unit entries to Database
		 */
		$units->map(function ($unit, $unit_id) {
			try {
				//retrieve existing unit or create new empty unit
				$newUnit = Unit::firstOrNew(['id' => $unit_id]);

				//insert new data or update existing
				$newUnit->id            = $unit_id;
				$newUnit->rarity_min    = $unit['rarity_min'];
				$newUnit->rarity_max    = $unit['rarity_max'];
				$newUnit->name          = $unit['name'];
				$newUnit->names         = $unit['names'];
				$newUnit->game_id       = $unit['game_id'];
				$newUnit->job_id        = $unit['job_id'];
				$newUnit->sex_id        = $unit['sex_id'];
				$newUnit->tribe_id      = $unit['tribe_id'];
				$newUnit->is_summonable = $unit['is_summonable'];
				$newUnit->TMR_type      = $unit['TMR'][0];
				$newUnit->TMR_id        = $unit['TMR'][1];
				$newUnit->equip         = json_encode($unit['equip']);
				$newUnit->skills        = array_key_exists('skills', $unit) ? $unit['skills'] : null;

				//save the new/updated unit
				$newUnit->save();

				//Collection of all unit entries
				$unitEntries = collect($unit['entries']);

				$unitEntries->map(function ($entry, $entry_id) use ($unit_id) {
					//retrieve existing unit or create new empty unit entry
					$newUnitEntry = UnitEntry::firstOrNew(['id' => $entry_id]);

					//insert new data or update existing
					$newUnitEntry->unit_id        = $unit_id;
					$newUnitEntry->compendium_id  = $entry['compendium_id'];
					$newUnitEntry->rarity         = $entry['rarity'];
					$newUnitEntry->growth_pattern = $entry['growth_pattern'];
					$newUnitEntry->stats          = $entry['stats'];
					$newUnitEntry->limitburst_id  = $entry['limitburst_id'];
					$newUnitEntry->attack_count   = $entry['attack_count'];
					$newUnitEntry->attack_frames  = json_encode($entry['attack_frames']);
					$newUnitEntry->effect_frames  = json_encode($entry['effect_frames']);
					$newUnitEntry->max_lb_drop    = $entry['max_lb_drop'];
					$newUnitEntry->ability_slots  = $entry['ability_slots'];
					$newUnitEntry->magic_affinity = json_encode($entry['magic_affinity']);
					$newUnitEntry->element_resist = json_encode($entry['element_resist']);
					$newUnitEntry->status_resist  = json_encode($entry['status_resist']);;
					$newUnitEntry->physical_resist     = $entry['physical_resist'];
					$newUnitEntry->magical_resist      = $entry['magical_resist'];
					$newUnitEntry->awakening_gil       = $entry['awakening']['gil'];
					$newUnitEntry->awakening_materials = $entry['awakening']['materials'];
					$newUnitEntry->strings             = $entry['strings'];

					//save the new/updated unit entry
					$newUnitEntry->save();


				});
			} catch (\ErrorException $e) {
				dd($e);
			}
		});
	}


	/**
	 *
	 */
	private function parseSkillsFile()
	{
		$skills = collect(json_decode(File::get(public_path('ffbe/skills.json')), true));

		$skills->map(function ($skill, $key) {

			try {
				//Retrieve existing skill by id or create new instance
				$newSkill = Skill::firstOrNew(['id' => $key]);


				//insert/update skill info
				$newSkill->type                  = $skill['type'];
				$newSkill->active                = $skill['active'];
				$newSkill->usable_in_exploration = array_key_exists('usable_in_exploration', $skill) ? $skill['usable_in_exploration'] : null;
				$newSkill->name                  = $skill['name'];
				$newSkill->compendium_id         = $skill['compendium_id'];
				$newSkill->rarity                = $skill['rarity'];
				$newSkill->magic_type            = array_key_exists('magic_type', $skill) ? $skill['magic_type'] : null;
				$newSkill->mp_cost               = $skill['mp_cost'];
				$newSkill->is_sealable           = array_key_exists('is_sealable', $skill) ? $skill['is_sealable'] : null;
				$newSkill->is_reflectable        = array_key_exists('is_reflectable', $skill) ? $skill['is_reflectable'] : null;
				$newSkill->attack_count          = json_encode($skill['attack_count']);
				$newSkill->attack_frames         = json_encode($skill['attack_frames']);
				$newSkill->effect_frames         = json_encode($skill['effect_frames']);
				$newSkill->move_type             = $skill['move_type'];
				$newSkill->effect_type           = $skill['effect_type'];
				$newSkill->attack_type           = $skill['attack_type'];
				$newSkill->element_inflict       = $skill['element_inflict'];
				$newSkill->effects               = $skill['effects'];
				$newSkill->effects_raw           = json_encode($skill['effects_raw']);
				if (array_key_exists('unit_restriction', $skill) && $skill['unit_restriction'] != null) {
					$newSkill->unit_restriction = json_encode($skill['unit_restriction']);
				}
				$newSkill->icon    = $skill['icon'];
				$newSkill->strings = $skill['strings'];

				//save skill
				$newSkill->save();
			} catch (\ErrorException $e) {
				dd($e);
			}
		});
	}


	/**
	 *
	 */
	private function parseEquipmentFile()
	{

		$equipment      = collect(json_decode(File::get(public_path('ffbe/equipment.json')), true));
		$equipmentTypes = collect();
		$equipmentSlots = collect();


		/**
		 * iterates through the equipment collection and get all types and slots with it's ids
		 */
		$equipment->map(function ($equipment) use ($equipmentTypes, $equipmentSlots) {
			$equipmentTypes->put($equipment['type_id'], $equipment['type']);
			$equipmentSlots->put($equipment['slot_id'], $equipment['slot']);
		});

		/**
		 * Save equipment types to database
		 */
		$equipmentTypes->map(function ($type, $key) {
			EquipmentType::firstOrCreate(['id' => $key, 'name' => $type]);
		});


		/**
		 * Save equipmen slots to database
		 */
		$equipmentSlots->map(function ($slot, $key) {
			EquipmentSlot::firstOrCreate(['id' => $key, 'name' => $slot]);
		});

		$equipment->map(function ($equipment, $key) {
			try {
				$newEquipment = Equipment::firstOrNew(['id' => $key]);

				$newEquipment->name             = $equipment['name'];
				$newEquipment->compendium_id    = $equipment['compendium_id'];
				$newEquipment->compendium_shown = $equipment['compendium_shown'];
				$newEquipment->rarity           = $equipment['rarity'];
				$newEquipment->type_id          = $equipment['type_id'];
				$newEquipment->slot_id          = $equipment['slot_id'];
				$newEquipment->is_twohanded     = $equipment['is_twohanded'];
				if (array_key_exists('dmg_variance', $equipment) && $equipment['dmg_variance'] != null) {
					$newEquipment->dmg_variance = json_encode($equipment['dmg_variance']);
				}
				$newEquipment->accuracy         = $equipment['accuracy'];
				$newEquipment->requirement_type = $equipment['requirements'][0];
				$newEquipment->requirement_id   = $equipment['requirements'][1];
				$newEquipment->skills           = $equipment['skills'];
				if (array_key_exists('effects', $equipment) && $equipment['effects'] != null) {
					$newEquipment->effects = $equipment['effects'];
				}
				$newEquipment->stats      = $equipment['stats'];
				$newEquipment->price_buy  = $equipment['price_buy'];
				$newEquipment->price_sell = $equipment['price_sell'];
				$newEquipment->icon       = $equipment['icon'];
				$newEquipment->strings    = $equipment['strings'];


				//save equipment
				$newEquipment->save();

			} catch (\ErrorException $e) {
				dd($e);
			}
		});
	}


	/**
	 *
	 */
	public function parseWorldsFile()
	{

		$worlds = collect(json_decode(File::get(public_path('ffbe/worlds.json')), true));


		$worlds->map(function ($world, $world_id) {
			try {
				$newWorld = World::firstOrNew(['id' => $world_id]);

				$newWorld->names = $world['names'];

				$newWorld->save();

				//Collection of all world regions
				$worldRegions = collect($world['regions']);

				$worldRegions->map(function ($region, $region_id) use ($world_id) {
					//Insert regions into database
					$newRegion           = Region::firstOrNew(['id' => $region_id]);
					$newRegion->names    = $region['names'];
					$newRegion->world_id = $world_id;

					$newRegion->save();

					$regionSubregions = collect($region['subregions']);

					//insert subregions
					$regionSubregions->map(function ($subregion, $subregion_id) use ($region_id) {
						$newSubregion        = Subregion::firstOrNew(['id' => $subregion_id]);
						$newSubregion->names = $subregion['names'];
						$newSubregion->region_id = $region_id;
						$newSubregion->save();
					});
				});

			} catch (\ErrorException $e) {
				dd($e);
			}

		});
	}

	/**
	 *
	 */
	public function parseTownsFile()
	{

		$towns = collect(json_decode(File::get(public_path('ffbe/towns.json')), true));


		$towns->map(function ($town, $town_id) {
			try {
				$newTown = Town::firstOrNew(['id' => $town_id]);

				$newTown->names        = $town['names'];
				$newTown->subregion_id = $town['subregion_id'];
				$newTown->position     = $town['position'];
				$newTown->icon         = $town['icon'];

				$newTown->save();

			} catch (\ErrorException $e) {
				dd($e);
			}

		});
	}

	/**
	 *
	 */
	private function parseDungeonsFile()
	{

		$dungeons = collect(json_decode(File::get(public_path('ffbe/dungeons.json')), true));

		$dungeons->map(function ($dungeon, $dungeon_id) {
			try {
				$newDungeon = Dungeon::firstOrNew(['id' => $dungeon_id]);


				$newDungeon->names        = array_key_exists('names', $dungeon) ? $dungeon['names'] : null;
				$newDungeon->subregion_id = array_key_exists('subregion_id', $dungeon) ? $dungeon['subregion_id'] : null;
				$newDungeon->type         = array_key_exists('type', $dungeon) ? $dungeon['type'] : null;
				$newDungeon->position     = array_key_exists('position', $dungeon) ? $dungeon['position'] : null;
				$newDungeon->icon         = array_key_exists('icon', $dungeon) ? $dungeon['icon'] : null;

				//save dungeon
				$newDungeon->save();

			} catch (\ErrorException $e) {
				dd($e);
			}
		});
	}


	/**
	 *
	 */
	private function parseMissionsFile()
	{

		$missions = collect(json_decode(File::get(public_path('ffbe/missions.json')), true));

		$missions->map(function ($mission, $mission_id) {
			try {
				$newMission = Mission::firstOrNew(['id' => $mission_id]);

				$newMission->dungeon_id = array_key_exists('dungeon_id', $mission) ? $mission['dungeon_id'] : null;
				$newMission->name       = array_key_exists('name', $mission) ? $mission['name'] : null;
				$newMission->type       = array_key_exists('type', $mission) ? $mission['type'] : null;
				$newMission->wave_count = array_key_exists('wave_count', $mission) ? $mission['wave_count'] : null;
				$newMission->cost_type  = array_key_exists('cost_type', $mission) ? $mission['cost_type'] : null;
				$newMission->cost       = array_key_exists('cost', $mission) ? $mission['cost'] : null;
				$newMission->flags      = array_key_exists('flags', $mission) ? $mission['flags'] : null;
				$newMission->reward     = array_key_exists('reward', $mission) ? $mission['reward'] : null;
				$newMission->gil        = array_key_exists('gil', $mission) ? $mission['gil'] : null;
				$newMission->exp        = array_key_exists('exp', $mission) ? $mission['exp'] : null;
				$newMission->challenges = $mission['challenges'] != [] ? $mission['challenges'] : null;

				$newMission->save();
			} catch (\ErrorException $e) {
				dd($e);
			}
		});
	}


	/**
	 *
	 */
	public function parseItemsFile()
	{

		$items = collect(json_decode(File::get(public_path('ffbe/items.json')), true));


		$items->map(function ($item, $item_id) {
			try {
				$newItem = Item::firstOrNew(['id' => $item_id]);

				$newItem->name                  = $item['name'];
				$newItem->type                  = $item['type'];
				$newItem->compendium_id         = $item['compendium_id'];
				$newItem->compendium_shown      = $item['compendium_shown'];
				$newItem->usable_in_combat      = $item['usable_in_combat'];
				$newItem->usable_in_exploration = $item['usable_in_exploration'];
				$newItem->flags                 = json_encode($item['flags']);
				$newItem->carry_limit           = $item['carry_limit'];
				$newItem->stack_size            = $item['stack_size'];
				$newItem->price_buy             = $item['price_buy'];
				$newItem->price_sell            = $item['price_sell'];
				$newItem->effects               = $item['effects'];
				$newItem->effects_raw           = json_encode($item['effects_raw']);
				$newItem->icon                  = $item['icon'];
				$newItem->strings               = $item['strings'];

				$newItem->save();
			} catch (\ErrorException $e) {
				dd($e);
			}

		});
	}


	/**
	 *
	 */
	public function parseLimitBurstsFile()
	{

		$limibursts = collect(json_decode(File::get(public_path('ffbe/limitbursts.json')), true));

		$limibursts->map(function ($limitburst, $limitburst_id) {
			try {
				$newLimitburst = Limitburst::firstOrNew(['id' => $limitburst_id]);


				$newLimitburst->name            = $limitburst['name'];
				$newLimitburst->cost            = $limitburst['cost'];
				$newLimitburst->attack_count    = json_encode($limitburst['attack_count']);
				$newLimitburst->attack_frames   = json_encode($limitburst['attack_frames']);
				$newLimitburst->effect_frames   = json_encode($limitburst['effect_frames']);
				$newLimitburst->move_type       = $limitburst['move_type'];
				$newLimitburst->damage_type     = $limitburst['damage_type'];
				$newLimitburst->element_inflict = $limitburst['element_inflict'];
				$newLimitburst->levels          = $limitburst['levels'];
				$newLimitburst->min_level       = $limitburst['min_level'];
				$newLimitburst->max_level       = $limitburst['max_level'];
				$newLimitburst->strings         = $limitburst['strings'];

				$newLimitburst->save();
			} catch (\ErrorException $e) {
				dd($e);
			}

		});
	}


	/**
	 *
	 */
	public function parseMateriaFile()
	{

		$materia = collect(json_decode(File::get(public_path('ffbe/materia.json')), true));

		$materia->map(function ($materia, $materia_id) {
			try {
				$newMateria = Materia::firstOrNew(['id' => $materia_id]);


				$newMateria->name          = $materia['name'];
				$newMateria->compendium_id = $materia['compendium_id'];
				$newMateria->skills        = $materia['skills'];
				$newMateria->effects       = $materia['effects'];
				$newMateria->unique        = $materia['unique'];
				$newMateria->price_buy     = $materia['price_buy'];
				$newMateria->price_sell    = $materia['price_buy'];
				$newMateria->icon          = $materia['icon'];
				$newMateria->strings       = $materia['strings'];

				$newMateria->save();
			} catch (\ErrorException $e) {
				dd($e);
			}

		});
	}


	/**
	 *
	 */
	public function parseEnhancementsFile()
	{

		$enhancements = collect(json_decode(File::get(public_path('ffbe/enhancements.json')), true));

		$enhancements->map(function ($enhancement, $enhancement_id) {
			try {
				$newEnhancement = Enhancement::firstOrNew(['id' => $enhancement_id]);


				$newEnhancement->name         = $enhancement['name'];
				$newEnhancement->skill_id_old = $enhancement['skill_id_old'];
				$newEnhancement->skill_id_new = $enhancement['skill_id_new'];
				$newEnhancement->cost         = $enhancement['cost'];
				$newEnhancement->units        = $enhancement['units'];
				$newEnhancement->strings      = $enhancement['strings'];

				$newEnhancement->save();
			} catch (\ErrorException $e) {
				dd($e);
			}

		});
	}


	/**
	 *
	 */
	public function parseExpeditionsFile()
	{

		$expeditions = collect(json_decode(File::get(public_path('ffbe/expeditions.json')), true));

		$expeditions->map(function ($expedition, $expedition_id) {
			try {
				$newExpedition = Expedition::firstOrNew(['id' => $expedition_id]);

				$newExpedition->name       = $expedition['name'];
				$newExpedition->type       = $expedition['type'];
				$newExpedition->cost       = $expedition['cost'];
				$newExpedition->rank       = $expedition['rank'];
				$newExpedition->difficulty = $expedition['difficulty'];
				$newExpedition->duration   = $expedition['duration'];
				$newExpedition->units      = $expedition['units'];
				if (array_key_exists('required', $expedition) && $expedition['required'] != null) {
					$newExpedition->required = $expedition['required'];
				}
				$newExpedition->next_id        = $expedition['next_id'];
				$newExpedition->reward         = $expedition['reward'];
				$newExpedition->relics         = $expedition['relics'];
				$newExpedition->exp_levels     = $expedition['exp_levels'];
				$newExpedition->unit_bonus     = $expedition['unit_bonus'];
				$newExpedition->unit_bonus_max = $expedition['unit_bonus_max'];
				if (array_key_exists('item_bonus', $expedition) && $expedition['item_bonus'] != null) {
					$newExpedition->item_bonus = $expedition['item_bonus'];
				}
				$newExpedition->stat_bonus = $expedition['stat_bonus'];
				$newExpedition->strings    = $expedition['strings'];

				$newExpedition->save();
			} catch (\ErrorException $e) {
				dd($e);
			}

		});
	}


	/**
	 *
	 */
	public function parseMogkingFile()
	{

		$mogKings = collect(json_decode(File::get(public_path('ffbe/mog_king.json')), true));

		$mogKings->map(function ($mogking, $mogking_id) {
			try {
				$mogkingRewards = collect($mogking);

				$mogkingRewards->map(function ($reward) use ($mogking_id) {
					$newMogking = new Mogking();

					$newMogking->currency_item  = $mogking_id;
					$newMogking->reward_name    = $reward['name'];
					$newMogking->reward_type    = $reward['reward_type'];
					$newMogking->reward_id      = $reward['reward_id'];
					$newMogking->currency_price = $reward['price'];
					$newMogking->amount         = $reward['amount'];

					$newMogking->save();
				});


			} catch (\ErrorException $e) {
				dd($e);
			}

		});
	}


	/**
	 *
	 */
	public function parseRecipesFile()
	{

		$recipes = collect(json_decode(File::get(public_path('ffbe/recipes.json')), true));

		$recipes->map(function ($recipe, $recipe_id) {
			try {
				$newRecipe = Recipe::firstOrNew(['id' => $recipe_id]);


				$newRecipe->name             = $recipe['name'];
				$newRecipe->compendium_id    = $recipe['compendium_id'];
				$newRecipe->compendium_shown = $recipe['compendium_shown'];
				$newRecipe->item             = $recipe['item'];
				$newRecipe->time             = $recipe['time'];
				$newRecipe->mats             = json_encode($recipe['mats']); //TODO: Parse item type, item and quantity to separate table fields
				$newRecipe->count            = $recipe['count'];

				$newRecipe->save();
			} catch (\ErrorException $e) {
				dd($e);
			}

		});
	}


	/**
	 *
	 */
	public function parseSummonsFile()
	{

		$summons = collect(json_decode(File::get(public_path('ffbe/summons.json')), true));

		$summons->map(function ($summon, $summon_id) {
			try {
				$newSummon = Summon::firstOrNew(['id' => $summon_id]);


				$newSummon->names = $summon['names'];
				$newSummon->image = $summon['image'];
				$newSummon->icon  = $summon['icon'];
				//TODO: Save summon skills to new table
				$newSummon->skill = $summon['skill'];
				$newSummon->color = $summon['color'];
				//TODO: Save summon entries to new table
				$newSummon->entries = $summon['entries'];

				$newSummon->save();
			} catch (\ErrorException $e) {
				dd($e);
			}

		});
	}

	//TODO: Parse Summonboards.json, after extracting skills and summon entries to new tables
}
