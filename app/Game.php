<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Game
 *
 * @package App
 * @property int                                                       $id
 * @property string                                                    $name
 * @property \Carbon\Carbon|null                                       $created_at
 * @property \Carbon\Carbon|null                                       $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Unit[] $units
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Game extends Model
{

	/**
	 * @var array
	 */
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	/**
	 *
	 */
	const GAME_COLLABORATION = [
		'10001' => 'FFI',
		'10002' => 'FFII',
		'10003' => 'FFIII',
		'10004' => 'FFIV',
		'10005' => 'FFV',
		'10006' => 'FFVI',
		'10007' => 'FFVII',
		'10009' => 'FFIX',
		'10010' => 'FFX',
		'10011' => 'FFXI',
		'10012' => 'FFXII',
		'10013' => 'FFXIII',
		'10014' => 'FFXIV',
		'10015' => 'FFXV',
		'11001' => 'FFBE',
		'11002' => 'FFT',
		'11003' => 'FF TYPE-0',
		'20001' => 'Terra Battle',
		'20002' => 'Imperial SaGa',
		'20003' => 'Brave Frontier',
		'20004' => 'Crystal Defenders',
		'20005' => 'Secret of Mana',
		'20006' => 'null',
		'20007' => 'null',
		'20008' => 'null',
		'20010' => 'NieR:Automata',
		'20011' => 'null',
		'20012' => 'null',
		'90001' => 'Ariana Grande',
	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function units()
	{
		return $this->hasMany(Unit::class);
	}
}
