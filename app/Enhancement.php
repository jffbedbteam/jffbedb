<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Enhancement
 *
 * @property int                 $id
 * @property string              $name
 * @property int                 $skill_id_old
 * @property int                 $skill_id_new
 * @property mixed               $cost
 * @property mixed               $units
 * @property string              $strings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereSkillIdNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereSkillIdOld($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereStrings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereUnits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enhancement whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Enhancement extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'cost'    => 'collection',
		'units'   => 'collection',
		'strings' => 'collection',
	];
}
