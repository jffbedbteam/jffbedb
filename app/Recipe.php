<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Recipe
 *
 * @property int                 $id
 * @property string              $name
 * @property int                 $compendium_id
 * @property int                 $compendium_shown
 * @property string              $item
 * @property int                 $time
 * @property mixed               $mats
 * @property int                 $count
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereCompendiumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereCompendiumShown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereMats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Recipe extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];
}
