<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Region
 *
 * @property int                                                            $id
 * @property string                                                         $name
 * @property \Carbon\Carbon|null                                            $created_at
 * @property \Carbon\Carbon|null                                            $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Region whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Region whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Region whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subregion[] $subregions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Dungeon[] $dungeons
 * @property-read \App\World $world
 * @property mixed $names
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Region whereNames($value)
 */
class Region extends Model
{
	protected $guarded = [];

	protected $hidden = ['created_at', 'updated_at'];

	protected $casts = [
		'names' => 'collection'
	];


	public function world()
	{
		return $this->belongsTo(World::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function subregions()
	{
		return $this->hasMany(Subregion::class);
	}
}
