<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitEntriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unit_entries', function (Blueprint $table) {
			$table->integer('id')->primary();
			$table->integer('unit_id');
			$table->mediumInteger('compendium_id');
			$table->smallInteger('rarity');
			$table->smallInteger('growth_pattern');
			$table->json('stats');
			$table->integer('limitburst_id')->nullable();
			$table->smallInteger('attack_count');
			$table->json('attack_frames');
			$table->json('effect_frames');
			$table->smallInteger('max_lb_drop');
			$table->smallInteger('ability_slots');
			$table->json('magic_affinity');
			$table->json('element_resist');
			$table->json('status_resist');
			$table->smallInteger('physical_resist');
			$table->smallInteger('magical_resist');
			$table->integer('awakening_gil')->nullable();
			$table->json('awakening_materials')->nullable();
			$table->json('strings');

			$table->timestamps();

			$table->foreign('unit_id')->references('id')->on('units');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('unit_entries');
	}
}
