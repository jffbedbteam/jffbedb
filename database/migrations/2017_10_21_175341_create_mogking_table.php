<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMogkingTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mogking', function (Blueprint $table) {
			$table->increments('id');
			$table->string('currency_item');
			$table->string('reward_name');
			$table->string('reward_type');
			$table->bigInteger('reward_id');
			$table->integer('currency_price');
			$table->integer('amount');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('mogkings');
	}
}
