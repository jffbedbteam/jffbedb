<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('missions', function (Blueprint $table) {

			$table->integer('id')->primary();
			$table->integer('dungeon_id')->nullable();
			$table->string('name')->nullable();
			$table->string('type')->nullable();
			$table->smallInteger('wave_count')->nullable();
			$table->string('cost_type')->nullable();
			$table->smallInteger('cost')->nullable();
			$table->json('flags')->nullable();
			$table->json('reward')->nullable();
			$table->integer('gil')->nullable();
			$table->integer('exp')->nullable();
			$table->json('challenges')->nullable();


			$table->foreign('dungeon_id')->references('id')->on('dungeons');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('missions');
	}
}
