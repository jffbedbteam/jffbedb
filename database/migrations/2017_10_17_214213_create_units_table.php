<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('units', function (Blueprint $table) {
			$table->integer('id')->primary();
			$table->smallInteger('rarity_min');
			$table->smallInteger('rarity_max');
			$table->string('name');
			$table->json('names');
			$table->integer('game_id');
			$table->integer('job_id');
			$table->smallInteger('sex_id');
			$table->smallInteger('tribe_id');
			$table->boolean('is_summonable');
			$table->string('TMR_type')->nullable();
			$table->integer('TMR_id')->nullable();
			$table->json('equip');
			$table->json('skills')->nullable();

			$table->timestamps();

			//Foreigns
			$table->foreign('game_id')->references('id')->on('games');
			$table->foreign('job_id')->references('id')->on('jobs');
			$table->foreign('sex_id')->references('id')->on('sexes');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('units');
	}
}
