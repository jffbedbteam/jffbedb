<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpeditionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('expeditions', function (Blueprint $table) {
			$table->integer('id')->primary();
			$table->string('name');
			$table->smallInteger('type');
			$table->integer('cost');
			$table->string('rank');
			$table->integer('difficulty');
			$table->integer('duration');
			$table->smallInteger('units');
			$table->json('required')->nullable();
			$table->integer('next_id')->nullable();
			$table->json('reward');
			$table->smallInteger('relics');
			$table->json('exp_levels');
			$table->json('unit_bonus');
			$table->smallInteger('unit_bonus_max');
			$table->json('item_bonus')->nullable();
			$table->json('stat_bonus');
			$table->json('strings');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('expeditions');
	}
}
