<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSummonsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('summons', function (Blueprint $table) {
			$table->smallInteger('id')->primary();
			$table->json('names')->nullable();
			$table->string('image');
			$table->string('icon');
			$table->json('skill');
			$table->json('color')->nullable();
			$table->json('entries');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('summons');
	}
}
