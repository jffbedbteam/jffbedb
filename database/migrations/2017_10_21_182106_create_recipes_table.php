<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recipes', function (Blueprint $table) {
			$table->bigInteger('id')->primary();
			$table->string('name');
			$table->integer('compendium_id')->nullable();
			$table->boolean('compendium_shown')->nullable();
			$table->string('item'); //first 2 digits (20 = items, 21 = Ability, 22 = Equipment)
			$table->integer('time');
			$table->json('mats');
			$table->smallInteger('count');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('recipes');
	}
}
