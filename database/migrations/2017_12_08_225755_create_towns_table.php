<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('towns', function (Blueprint $table) {
			$table->integer('id')->primary();
			$table->json('names');
			$table->integer('subregion_id')->nullable();
			$table->json('position')->nullable();
			$table->string('icon')->nullable();

			$table->timestamps();

			$table->foreign('subregion_id')->references('id')->on('subregions');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('towns');
    }
}
