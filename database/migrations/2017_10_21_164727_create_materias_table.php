<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMateriasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('materias', function (Blueprint $table) {
			$table->bigInteger('id')->primary();
			$table->string('name');
			$table->integer('compendium_id');
			$table->json('skills');
			$table->json('effects');
			$table->boolean('unique');
			$table->integer('price_buy');
			$table->integer('price_sell');
			$table->string('icon');
			$table->json('strings');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('materias');
	}
}
