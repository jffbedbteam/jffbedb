<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function (Blueprint $table) {
			$table->bigInteger('id')->primary();
			$table->string('name');
			$table->string('type');
			$table->integer('compendium_id');
			$table->boolean('compendium_shown');
			$table->boolean('usable_in_combat');
			$table->boolean('usable_in_exploration');
			$table->json('flags');
			$table->integer('carry_limit');
			$table->integer('stack_size');
			$table->integer('price_buy');
			$table->integer('price_sell');
			$table->json('effects')->nullable();
			$table->json('effects_raw')->nullable();
			$table->string('icon')->nullable();
			$table->json('strings');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('items');
	}
}
