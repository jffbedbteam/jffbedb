<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLimitburstsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('limitbursts', function (Blueprint $table) {
			$table->integer('id')->primary();
			$table->string('name');
			$table->smallInteger('cost');
			$table->json('attack_count');
			$table->json('attack_frames');
			$table->json('effect_frames');
			$table->smallInteger('move_type');
			$table->string('damage_type');
			$table->json('element_inflict')->nullable();
			$table->smallInteger('levels');
			$table->json('min_level');
			$table->json('max_level');
			$table->json('strings');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('limitbursts');
	}
}
