<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkillsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('skills', function (Blueprint $table) {
			$table->integer('id')->primary();
			$table->string('type');
			$table->boolean('active');
			$table->boolean('usable_in_exploration')->nullable();
			$table->string('name');
			$table->integer('compendium_id');
			$table->smallInteger('rarity');
			$table->string('magic_type')->nullable();
			$table->smallInteger('mp_cost');
			$table->boolean('is_sealable')->nullable();
			$table->boolean('is_reflectable')->nullable();
			$table->json('attack_count');
			$table->json('attack_frames');
			$table->json('effect_frames');
			$table->smallInteger('move_type');
			$table->json('effect_type');
			$table->string('attack_type');
			$table->json('element_inflict')->nullable();
			$table->json('effects');
			$table->json('effects_raw');
			$table->json('unit_restriction')->nullable();
			$table->string('icon');
			$table->json('strings');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('skills');
	}
}
