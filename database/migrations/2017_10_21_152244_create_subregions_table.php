<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubregionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subregions', function (Blueprint $table) {

			$table->integer('id')->primary();
			$table->json('names');
			$table->integer('region_id');

			$table->timestamps();

			$table->foreign('region_id')->references('id')->on('regions');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('subregions');
	}
}
