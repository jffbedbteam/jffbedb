<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDungeonsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dungeons', function (Blueprint $table) {
			$table->integer('id')->primary();
			$table->json('names')->nullable();
			$table->integer('subregion_id')->nullable();
			$table->string('type')->nullable();
			$table->json('position')->nullable();
			$table->string('icon')->nullable();


			$table->timestamps();

			$table->foreign('subregion_id')->references('id')->on('subregions');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('dungeons');
	}
}


