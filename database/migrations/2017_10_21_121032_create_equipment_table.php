<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('equipment', function (Blueprint $table) {
			$table->integer('id')->primary();
			$table->string('name');
			$table->integer('compendium_id');
			$table->boolean('compendium_shown');
			$table->smallInteger('rarity');
			$table->smallInteger('type_id');
			$table->smallInteger('slot_id');
			$table->boolean('is_twohanded');
			$table->json('dmg_variance')->nullable();
			$table->smallInteger('accuracy');
			$table->string('requirement_type')->nullable();
			$table->integer('requirement_id')->nullable();
			$table->json('skills')->nullable();
			$table->json('effects')->nullable();
			$table->json('stats');
			$table->integer('price_buy');
			$table->integer('price_sell');
			$table->string('icon')->nullable();
			$table->json('strings');


			$table->timestamps();

			$table->foreign('type_id')->references('id')->on('equipment_types');
			$table->foreign('slot_id')->references('id')->on('equipment_slots');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('equipment');
	}
}
