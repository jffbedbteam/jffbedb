<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//only for testing, job is scheduled in App/Console/Kernel
//route::get('databaseload', function(){
//	\App\Jobs\LoadFFBEjsonDatabase::dispatch();
//	return redirect('/');
//});