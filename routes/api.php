<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//handles all /api/units/xxxxx requests
Route::prefix('units')->group(function () {
	//List method, it's registered BEFORE apiResource to avoid overlapping
	Route::get('list', 'UnitController@list')->name('units.list');

	//find unit by name
	Route::get('name/{name}', 'UnitController@showByName')->name('units.showByName');
});


//API resources routes
Route::apiResource('units', 'UnitController')->only(['index', 'show']);
Route::apiResource('skills', 'SkillController')->only(['index', 'show']);
Route::apiResource('dungeons', 'DungeonController')->only(['index', 'show']);
Route::apiResource('enhancements', 'EnhancementController')->only(['index', 'show']);
Route::apiResource('equipment', 'EquipmentController')->only(['index', 'show']);
Route::apiResource('equipmentslots', 'EquipmentSlotController')->only(['index', 'show']);
Route::apiResource('equipmenttypes', 'EquipmentTypeController')->only(['index', 'show']);
Route::apiResource('expeditions', 'ExpeditionController')->only(['index', 'show']);
Route::apiResource('games', 'GameController')->only(['index', 'show']);
Route::apiResource('items', 'ItemController')->only(['index', 'show']);
Route::apiResource('jobs', 'JobController')->only(['index', 'show']);
Route::apiResource('limitbursts', 'LimitburstController')->only(['index', 'show']);
Route::apiResource('materias', 'MateriaController')->only(['index', 'show']);
Route::apiResource('missions', 'MissionController')->only(['index', 'show']);
Route::apiResource('mogkings', 'MogkingController')->only(['index', 'show']);
Route::apiResource('recipes', 'RecipeController')->only(['index', 'show']);
Route::apiResource('regions', 'RegionController')->only(['index', 'show']);
Route::apiResource('sexes', 'SexController')->only(['index', 'show']);
Route::apiResource('subregions', 'SubregionController')->only(['index', 'show']);
Route::apiResource('summonboards', 'SummonBoardController')->only(['index', 'show']);
Route::apiResource('summons', 'SummonController')->only(['index', 'show']);
Route::apiResource('unitentries', 'UnitEntryController')->only(['index', 'show']);
Route::apiResource('worlds', 'WorldController')->only(['index', 'show']);
Route::apiResource('towns', 'TownController')->only(['index', 'show']);